package ejercicio4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ex4Visible extends JFrame {

	private JPanel contentPane;
	private JTextField operador1;
	private JTextField operador2;
	private JTextField textField;
	private JLabel labelCanviante;
	/**
	 * Launch the application.
	 */
	public void ejecutar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex4Visible frame = new ex4Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public ex4Visible() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		operador1 = new JTextField();
		operador1.setHorizontalAlignment(SwingConstants.CENTER);
		operador1.setBounds(10, 47, 180, 37);
		contentPane.add(operador1);
		operador1.setColumns(10);
		
		operador2 = new JTextField();
		operador2.setHorizontalAlignment(SwingConstants.CENTER);
		operador2.setColumns(10);
		operador2.setBounds(244, 47, 180, 37);
		contentPane.add(operador2);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setColumns(10);
		textField.setBounds(125, 213, 180, 37);
		contentPane.add(textField);
		
		labelCanviante = new JLabel("");
		labelCanviante.setHorizontalAlignment(SwingConstants.CENTER);
		labelCanviante.setFont(new Font("Tahoma", Font.PLAIN, 24));
		labelCanviante.setBounds(189, 47, 56, 37);
		contentPane.add(labelCanviante);
		
		JButton btnSuma = new JButton("+");
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!operador1.getText().isEmpty() && !operador2.getText().isEmpty()) {
					labelCanviante.setText("+");
					double result = 0;
					result = Integer.parseInt(operador1.getText()) + Integer.parseInt(operador2.getText());
					textField.setText(String.valueOf(result));					
				}		
			}
		});
	
		btnSuma.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnSuma.setBounds(10, 113, 65, 46);
		contentPane.add(btnSuma);
		
		JButton btnResta = new JButton("-");
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!operador1.getText().isEmpty() && !operador2.getText().isEmpty()) {
					labelCanviante.setText("-");
					double result = 0;
					result = Integer.parseInt(operador1.getText()) - Integer.parseInt(operador2.getText());
					textField.setText(String.valueOf(result));					
				}	
			}
		});
		btnResta.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnResta.setBounds(125, 113, 65, 46);
		contentPane.add(btnResta);
		
		JButton btnMulti = new JButton("X");
		btnMulti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!operador1.getText().isEmpty() && !operador2.getText().isEmpty()) {
					labelCanviante.setText("X");
					double result = 0;
					result = Integer.parseInt(operador1.getText()) * Integer.parseInt(operador2.getText());
					textField.setText(String.valueOf(result));					
				}	
				
			}
		});
	
		btnMulti.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnMulti.setBounds(244, 113, 61, 46);
		contentPane.add(btnMulti);
		
		JButton btnDiv = new JButton("\u00F7");
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!operador1.getText().isEmpty() && !operador2.getText().isEmpty()) {
					labelCanviante.setText("/");
					double result = 0;
					result = Integer.parseInt(operador1.getText()) / Integer.parseInt(operador2.getText());
					textField.setText(String.valueOf(result));					
				}	
				
			}
		});
	
		btnDiv.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnDiv.setBounds(359, 113, 65, 46);
		contentPane.add(btnDiv);
		
		JLabel lblNewLabel = new JLabel("Resultado:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(125, 184, 180, 21);
		contentPane.add(lblNewLabel);
		
		JLabel lblOperador = new JLabel("Operador 1:");
		lblOperador.setHorizontalAlignment(SwingConstants.LEFT);
		lblOperador.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblOperador.setBounds(10, 15, 180, 21);
		contentPane.add(lblOperador);
		
		JLabel lblOperador_2 = new JLabel("Operador 2:");
		lblOperador_2.setHorizontalAlignment(SwingConstants.LEFT);
		lblOperador_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblOperador_2.setBounds(244, 15, 180, 21);
		contentPane.add(lblOperador_2);
		
		JButton btnSalir = new JButton("Cerrar");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(335, 227, 89, 23);
		contentPane.add(btnSalir);
		
	}


}
