package ejercicio3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
public class ex3Visible extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void ejecutar(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex3Visible frame = new ex3Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ex3Visible() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Sistema operativo:");
		lblNewLabel.setBounds(20, 50, 94, 14);
		contentPane.add(lblNewLabel);
		
		
		
		
		JLabel lblEncuestaEspecialidad = new JLabel("Encuesta grafica");
		lblEncuestaEspecialidad.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblEncuestaEspecialidad.setBounds(10, 11, 180, 28);
		contentPane.add(lblEncuestaEspecialidad);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Windows");
		rdbtnNewRadioButton.setSelected(true);
		rdbtnNewRadioButton.setBounds(30, 71, 109, 23);
		rdbtnNewRadioButton.setActionCommand("Windows");
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Linux");
		rdbtnNewRadioButton_1.setBounds(30, 97, 109, 23);
		rdbtnNewRadioButton_1.setActionCommand("Linux");
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Mac");
		rdbtnNewRadioButton_2.setBounds(30, 123, 109, 23);
		rdbtnNewRadioButton_2.setActionCommand("Mac");
		contentPane.add(rdbtnNewRadioButton_2);
		
		ButtonGroup sistemasOperativos = new ButtonGroup();
		sistemasOperativos.add(rdbtnNewRadioButton);
		sistemasOperativos.add(rdbtnNewRadioButton_1);
		sistemasOperativos.add(rdbtnNewRadioButton_2);
		
		JLabel lblNewLabel_1 = new JLabel("Especialidad:");
		lblNewLabel_1.setBounds(209, 50, 66, 14);
		contentPane.add(lblNewLabel_1);
		
		JCheckBox checkProgramacion = new JCheckBox("Programacion");
		checkProgramacion.setBounds(219, 71, 109, 23);
		checkProgramacion.setActionCommand("programacion");
		contentPane.add(checkProgramacion);
		
		JCheckBox checkGrafico = new JCheckBox("Dise\u00F1o grafico");
		checkGrafico.setBounds(219, 97, 109, 23);
		checkGrafico.setActionCommand("dise�o grafico");
		contentPane.add(checkGrafico);
		
		JCheckBox checkAdmin = new JCheckBox("Administracion");
		checkAdmin.setBounds(219, 123, 126, 23);
		checkAdmin.setActionCommand("administracion");
		contentPane.add(checkAdmin);
		
		JButton btnNewButton = new JButton("Enviar encuesta");
		btnNewButton.setBounds(274, 227, 135, 23);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String SO=sistemasOperativos.getSelection().getActionCommand();
				String especialista="nada";
				if(checkProgramacion.isSelected()) {
					especialista=checkProgramacion.getText();
				}
				if(checkGrafico.isSelected()) {
					if(especialista.equals("nada")) {
						especialista=checkGrafico.getText();
					} else {
						especialista=especialista+", "+checkGrafico.getText();
					}
				}
				if(checkAdmin.isSelected()) {
					if(especialista.equals("nada")) {
						especialista=checkAdmin.getText();
					} else {
						especialista=especialista+", "+checkAdmin.getText();
					}
				}
				JOptionPane.showMessageDialog(null, "El cliente usa "+SO+" como sistema operativo y es especialista en "+especialista);
			}
		});
	}
}
