package ejercicio2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class Ex2Vista {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	private JTextField tfile;
	private JButton btnAdd;
	private JComboBox<String> cPelis;

	/**
	 * Launch the application.
	 */
	public void eject() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex2Vista window = new Ex2Vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ex2Vista() {
		initialize();
		
		
	}
	/**
	 * Crear el metodo action para el a�adir
	 * @param e
	 */

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		tfile = new JTextField();
		tfile.setBounds(47, 103, 134, 28);
		frame.getContentPane().add(tfile);
		tfile.setColumns(10);
		
		btnAdd = new JButton("A\u00F1adir");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 cPelis.addItem(tfile.getText());
			}
		});
		btnAdd.setBounds(57, 152, 89, 23);
		frame.getContentPane().add(btnAdd);
		
		cPelis = new JComboBox();
		cPelis.setBounds(256, 105, 134, 24);
		frame.getContentPane().add(cPelis);
		
		JLabel lblNewLabel = new JLabel("Escribe el titulo de una pelicula");
		lblNewLabel.setBounds(37, 57, 199, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Peliculas");
		lblNewLabel_1.setBounds(300, 57, 64, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
	}
}
